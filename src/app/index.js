import AuthModule from './api/auth/module';
import UserModule from './api/user/module';
import RoleModule from './api/role/module';

export default function Router(router) {
  const modules = [];
  const auth = AuthModule(router);
  const user = UserModule(router);
  const role = RoleModule(router);

  modules.push(auth);
  modules.push(user);
  modules.push(role);

  modules.forEach((module) => {
    module.createEndpoints();
  });
  return router;
}

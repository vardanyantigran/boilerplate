class Service {
  constructor() {
    this.service = 'Service';
  }

  async store(data) {
    // console.log(this)
    const response = await this.repository.store(data);
    return response;
  }

  async getByPk(pk) {
    // console.log(this)
    const response = this.repository.getByPk(pk);
    return response;
  }
}

export default Service;

class Validator {
  constructor() {
    this.validator = 'Validator';
    this.schema = {
        email: {
          in: ['body'],
          errorMessage: 'Please enter a valid email address', // TODO: STATIC_TEXT
          isEmail : true,
          custom: {
            options: (email) => this.isExistByParameter(email, 'email', this.repository)
          }
        }
    }
  }

  async isExistByParameter(search, by, repository) {
    const response = !!await repository.findOne(search, by);
    return response;
  }
}

export default Validator;

export function injectRepository(modelName, ModuleRepository) {
  return function(classRef, propName, propDescriptors){
    propDescriptors.initializer = () => new ModuleRepository(modelName);
  }
}

export function injectService(ModuleService) {
  return function(classRef, propName, propDescriptors){
    propDescriptors.initializer = () => new ModuleService;
  }
}

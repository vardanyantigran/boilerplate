import { Op } from "sequelize";
import models from '../models/index.js';

class Repository {
  constructor(modelName) {
    this.models = models;
    this.model = models[modelName];
    this.transaction = models.sequelize.transaction;
    this.Op = Op;
  }

  async startTransaction() {
    try {
      const transaction = await this.transaction();
      return transaction;
    } catch (error) {
      throw new Error(error);
    }
  }

  async commitTransaction(transaction) {
    try {
      const result = await transaction.commit();
      return result;
    } catch (error) {
      throw new Error(error);
    }
  }

  async rollbackTransaction(transaction) {
    try {
      const result = await transaction.rollback();
      return result;
    } catch (error) {
      throw new Error(error);
    }
  }

  async store(data) {
    try {
      const response = await this.model.create(data);
      return response;
    } catch (error) {
      throw new Error(error);
    }
  }

  async getByPk(pk) {
    try {
      const response = await this.model.findByPk(pk);
      return response;
    } catch (error) {
      throw new Error(error);
    }
  }

  async findOne(search, by) {
    try {
      const response = await this.model.findOne({
        where: { [by]: search }
      });
      return response;
    } catch (error) {
      throw new Error(error);
    }
  }
}

export default Repository;
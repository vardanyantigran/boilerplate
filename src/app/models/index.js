import fs from 'fs';
import path from 'path';
import Sequelize from 'sequelize';
import { exec } from 'child_process';

const db = {};
const force = false;

export function setUpDb(sequelize, ) {
  const basename = path.basename(__filename);
  fs.readdirSync(__dirname)
    .filter(file => (file.indexOf('.') !== 0) && (file !== basename) && (file.slice(-3) === '.js')).forEach((file) => {
      const model = sequelize.import(path.join(__dirname, file));
      db[model.name] = model;
    });
  Object.keys(db).forEach((modelName) => {
    if (db[modelName].associate) {
      db[modelName].associate(db);
    }
  });
  db.sequelize = sequelize;
  db.Sequelize = Sequelize;
  sequelize.sync({ force })
    .then(() => {
      force && exec('npm run seed', error => error && console.error(error)).stdout.pipe(process.stdout);
    });
}

export default db;

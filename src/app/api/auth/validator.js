import { body, validationResult, param, checkSchema } from 'express-validator/check';
import models from '../../models';
import Validator from '../../core/Validator';
import { injectRepository } from '../../core/injectors';
import UserRepository from '../user/repository';
import autobind from 'autobind-decorator';

@autobind
export default class AuthValidator extends Validator {
  constructor() {
    super()
  }
  @injectRepository('user', UserRepository) repository

  signIn = ((req, res, next) => {
    return checkSchema({
      email: this.schema.email,
      password: {
        isLength: {
          errorMessage: 'Password should be at least 6 chars long', // TODO: STATIC_TEXT
          options: { min: 6 } // TODO: STATIC_VALUE
        }
      }
    });
  })()
}

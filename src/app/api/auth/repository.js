import Repository from "../../core/Repository";

class AuthRepository extends Repository {
  constructor(modelName) {
    super(modelName);
  }
  async get(query) {
    try {
      const response = await this.model.findAll();
      return response;
    } catch (error) {
      throw new Error(error); // TODO: ERROR_HANDLER
    }
  }

  async findUser(email, password) {
    try {
      const user = await this.model.findOne({
        // attributes: { include: [ "password" ] }, // get password
        where: { [this.Op.and]: [
          { email },
          { password }
        ] },
        include: [{ model: this.models.role }]
      });
      return user ? user.get({ plain: true }) : false;
    } catch (error) {
      throw new Error(error); // TODO: ERROR_HANDLER
    }
  }
}

export default AuthRepository;

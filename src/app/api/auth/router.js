import AuthController from './controller';
import AuthValidator from './validator';
import { checkAuthentication } from '../../utils/auth';

export default (router) => {
  const validator = new AuthValidator;
  const controller = new AuthController;
  router.post('/', validator.signIn, controller.signIn);
  // ----------------------------------------------------------------------------------------------
  // router.get('/', checkAuthentication, controller.signOut);
};

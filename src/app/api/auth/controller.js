import crypto from 'crypto';
import uuid from 'uuid/v4';
import jwt from 'jsonwebtoken';
import autobind from 'autobind-decorator';
import models from '../../models';
import { AuthError } from '../../utils/errorHandler';
import * as redis from '../../utils/redis';
import { injectService } from '../../core/injectors';
import Controller from '../../core/Controller';
import AuthService from './service';
import { validationResult } from 'express-validator/check';

@autobind
class AuthController extends Controller {
  @injectService(AuthService) service

  async signIn (req, res, next) {
    try {
      validationResult(req).throw();
      const response = await this.service.signIn(req.body.email, req.body.password);
      return res.responseHandler(response);
    } catch (error) {
      return next(error);
    }
  }
  
  async store(req, res, next) {
    return res.responseHandler(this.service.store(req.body));
  }
}

export default AuthController;

// export async function signIn(req, res, next) {
//   try {
//     validationResult(req).throw();
    
//     // const userWrapper = await models.user.findOne({
//     //   attributes: { include: ['password'] },
//     //   where: { email: req.body.email },
//     //   include: [
//     //     { model: models.role },
//     //   ],
//     // });
//     // if (!userWrapper) {
//     //   throw new AuthError('Wrong username or password');
//     // }
//     // const { password, ...user } = userWrapper.get({ plain: true });
//     // if (password !== crypto.createHash('sha1').update(req.body.password).digest('hex')) {
//     //   throw new AuthError('Wrong username or password');
//     // }
    
//     // const sessionId = uuid();
//     // global.redisClient.set(sessionId, JSON.stringify(user));
//     // return res.responseHandler({
//     //   token: jwt.sign({ id: user.id, sessionId }, process.env.JWT_SECRET),
//     //   user,
//     // });
//     return res.responseHandler('controller')
//   } catch (error) {
//     return next(error);
//   }
// }

// export async function signOut(req, res, next) {
//   try {
//     const { sessionId } = jwt.verify(req.headers.authorization, process.env.JWT_SECRET);
//     await redis.del(sessionId);
//     return res.responseHandler(null, 'success logout');
//   } catch (error) {
//     return next(error);
//   }
// }


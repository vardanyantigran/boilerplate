import crypto from 'crypto';
import { injectRepository } from '../../core/injectors';
import Service from '../../core/Service';
import UserRepository from './repository';
import { AuthError } from '../../utils/errorHandler';
import uuid from 'uuid/v4';

class AuthService extends Service {
  @injectRepository('user', UserRepository) repository

  async signIn(email, password) {
    try {
      const passwordHash = crypto.createHash('sha1').update(password).digest('hex');
      const user = await this.repository.findUser(email, passwordHash);
      if(!user) {
        throw new AuthError('Wrong Password', ); // TODO: STATIC_TEXT
      }
      const sessionId = uuid();
      return user;
    } catch (error) {
      throw new Error(error); // TODO: ERROR_HANDLER
    }
    // const userWrapper = await models.user.findOne({
    //   attributes: { include: ['password'] },
    //   where: { email: req.body.email },
    //   include: [
    //     { model: models.role },
    //   ],
    // });
    // if (!userWrapper) {
    //   throw new AuthError('Wrong username or password');
    // }
    // const { password, ...user } = userWrapper.get({ plain: true });
    // if (password !== crypto.createHash('sha1').update(req.body.password).digest('hex')) {
    //   throw new AuthError('Wrong username or password');
    // }
    
    // const sessionId = uuid();
    // global.redisClient.set(sessionId, JSON.stringify(user));
    // return res.responseHandler({
    //   token: jwt.sign({ id: user.id, sessionId }, process.env.JWT_SECRET),
    //   user,
    // });
  }

  async store(data) {
    const passwordString = Math.random().toString(36).slice(-8);
    const password = crypto.createHash('sha1').update(passwordString).digest('hex')
    const response = await super.store({ ...data, password }, this.userRepository);
    return response;
  }

  async getByPk(pk) {
    const response = this.userRepository.getByPk(pk);
    return response;
  }

  async get(query, params, body) {
    Object.keys.map
    const response = this.userRepository.get(query);
  }
}

export default AuthService;

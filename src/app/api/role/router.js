import * as controller from './controller';
import * as validator from './validator';
import { checkAuthentication, checkPermissions } from '../../utils/auth';
import * as permissions from '../../utils/permissions';

export default (router) => {
  // =======================================================================================
  router.use(checkAuthentication);
  router.use(checkPermissions(permissions.ROLE_VIEW));
  // =======================================================================================
  router.get('/', controller.showAll);
  // ---------------------------------------------------------------------------------------
  router.get('/:id', validator.isExist, controller.show);
  // =======================================================================================
  router.use(checkPermissions(permissions.ROLE_EDIT));
  // =======================================================================================
  router.post('/', validator.store, controller.store);
  // ---------------------------------------------------------------------------------------
  router.put('/:id', validator.isExist, validator.store, controller.update);
  // ---------------------------------------------------------------------------------------
  router.delete('/:id', validator.isExist, controller.remove);
};

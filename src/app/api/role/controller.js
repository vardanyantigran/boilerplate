import models from '../../models';
import updatedData from '../../utils/sanitizer';

export async function store(req, res, next) {
  try {
    const role = await models.role.create(req.body);
    return res.responseHandler(role);
  } catch (error) {
    return next(error);
  }
}

export async function showAll(req, res, next) {
  try {
    const roles = await models.role.findAll();
    return res.responseHandler(roles);
  } catch (error) {
    return next(error);
  }
}

export async function update(req, res, next) {
  try {
    const role = await models.role.update(req.body, {
      where: {
        id: req.params.id,
      },
      returning: true,
    });
    return res.responseHandler(updatedData(role));
  } catch (error) {
    return next(error);
  }
}

export async function remove(req, res, next) {
  try {
    const role = await models.role.destroy({
      where: {
        id: req.params.id,
      },
    });
    return res.responseHandler(role);
  } catch (error) {
    return next(error);
  }
}

export async function show(req, res, next) {
  try {
    const role = await models.role.findByPk(req.params.id);
    return res.responseHandler(role);
  } catch (error) {
    return next(error);
  }
}

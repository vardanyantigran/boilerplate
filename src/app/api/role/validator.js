import { body, validationResult, param } from 'express-validator/check';
import models from '../../models';

async function isRoleNameAvailable(name) {
  return !await models.role.findOne({
    where: { name },
    attributes: ['id'],
  });
}

async function isRoleExist(id) {
  return !!await models.role.findByPk(parseInt(id));
}

export async function store(req, res, next) {
  try {
    await Promise.all([
      body('name').optional().custom(isRoleNameAvailable).withMessage(`Role name '${req.body.name}' alraedy exist`),
      body('value').isInt().withMessage(`Value must be an integer`)
    ].map(functionItem => functionItem(req, res, () => null)));
    validationResult(req).throw();
    return next();
  } catch (error) {
    return next(error);
  }
}

export async function isExist(req, res, next) {
  try {
    const id = parseInt(req.params.id);
    await Promise.all([
      param('id').custom(isRoleExist).withMessage(`Role with ID:'${id}' does not exist`),
      body('value').isInt().optional().withMessage(`Value must be an integer`)
    ].map(functionItem => functionItem(req, res, () => null)));
    validationResult(req).throw();
    return next();
  } catch (error) {
    return next(error);
  }
}
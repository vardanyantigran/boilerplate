import Repository from '../../core/Repository';

class UserRepository extends Repository {
  constructor(modelName) {
    super(modelName);
  }
  async get(query) {
    try {
      const response = await this.model.findAll();
      return response;
    } catch (error) {
      throw new Error(error);
    }
  }
}

export default UserRepository;

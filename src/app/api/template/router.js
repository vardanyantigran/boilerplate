import * as controller from './controller';
import * as validator from './validator';
import { checkAuthentication, checkPermissions } from '../../utils/auth';
import * as permissions from '../../utils/permissions';

export default (router) => {
  // =======================================================================================
  // router.use(checkAuthentication);
  // =======================================================================================
  router.get('/permissions', checkPermissions(permissions.PERMISSION), controller.getPermissions);
  // =======================================================================================
  // router.use(checkPermissions(permissions.USER_VIEW));
  // =======================================================================================
  router.get('/me', controller.showCurrentUser);
  // ---------------------------------------------------------------------------------------
  router.get('/', checkPermissions(permissions.USER_VIEW), controller.showAll);
  // ---------------------------------------------------------------------------------------
  // router.get('/:id', checkPermissions(permissions.USER_VIEW), validator.isExist, controller.show);
  router.get('/:id', validator.isExist, controller.get);
  // =======================================================================================
  router.use(checkPermissions(permissions.USER_EDIT));
  // =======================================================================================
  router.post('/', validator.store, controller.store);
  // ---------------------------------------------------------------------------------------
  router.put('/:id', validator.isExist, validator.update, controller.update);
  // ---------------------------------------------------------------------------------------
  router.delete('/:id', validator.isExist, controller.remove);
};

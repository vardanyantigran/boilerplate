import { body, validationResult, param } from 'express-validator/check';
import models from '../../models';

async function isEmailAvailable(email) {
  return !await models.user.findOne({
    where: { email },
    attributes: ['email'],
  });
}

async function isRoleAvailable(id) {
  return !!await models.user.findByPk(id);
}

async function isUserExist(id) {
  return !!await models.user.findByPk(id);
}

export async function store(req, res, next) {
  try {
    await Promise.all([
      body('email').custom(isEmailAvailable).withMessage(`This email '${req.body.email}' alraedy in use`),
      body('firstName').withMessage(`Value is required`),
      body('lastName').withMessage(`Value is required`),
      body('roleId').optional().custom(isRoleAvailable).withMessage(`This role '${req.body.roleId}' not exist`)
    ].map(functionItem => functionItem(req, res, () => null)));
    validationResult(req).throw();
    return next();
  } catch (error) {
    return next(error);
  }
}

export async function update(req, res, next) {
  try {
    await Promise.all([
      body('email').optional().custom(isEmailAvailable).withMessage(`This email '${req.body.email}' alraedy in use`),
      body('firstName').optional().withMessage(`Value is required`),
      body('lastName').optional().withMessage(`Value is required`),
      body('roleId').optional().custom(isRoleAvailable).withMessage(`This role '${req.body.roleId}' not exist`)
    ].map(functionItem => functionItem(req, res, () => null)));
    validationResult(req).throw();
    return next();
  } catch (error) {
    return next(error);
  }
}

export async function isExist(req, res, next) {
  try {
    await Promise.all([
      param('id').custom(isUserExist).withMessage(`User with ID:'${req.param.id}' does not exist`),
    ].map(functionItem => functionItem(req, res, () => null)));
    validationResult(req).throw();
    return next();
  } catch (error) {
    return next(error);
  }
}
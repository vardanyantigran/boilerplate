import crypto from 'crypto';
import { injectRepository } from '../../core/injectors';
import Service from '../../core/Service';
import UserRepository from './repository';

class UserService extends Service {
  @injectRepository('user', UserRepository) userRepository

  async store(data) {
    const passwordString = Math.random().toString(36).slice(-8);
    const password = crypto.createHash('sha1').update(passwordString).digest('hex')
    const response = await this.userRepository.store({ ...data, password });
    return response;
  }

  async getByPk(pk) {
    const response = this.userRepository.getByPk(pk);
    return response;
  }
}

export default UserService;

import Repository from '../../core/Repository';

class UserRepository extends Repository {
  constructor(modelName) {
    super(modelName);
  }
  async getByPk(pk) {
    try {
      const response = await this.model.findByPk(pk);
      return response;
    } catch (error) {
      throw new Error(error);
    }
  }
}

export default UserRepository;

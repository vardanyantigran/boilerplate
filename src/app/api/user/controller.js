import crypto from 'crypto';
import { Op } from 'sequelize';
import autobind from 'autobind-decorator'
import UserService from './service';
import models from '../../models';
import * as permissions from '../../utils/permissions';
import { sendNewUserPasswordEmail } from '../../utils/mailing';
import { injectService } from '../../core/injectors';
import Controller from '../../core/Controller';

@autobind
class UserController extends Controller {
  @injectService(UserService) service

  async get(req, res, next) {
    return res.responseHandler(this.service.getByPk(req.params.id));
  }
  
  async store(req, res, next) {
    return res.responseHandler(this.service.store(req.body));
  }
}

export default UserController;

// ===========================================================================

// export async function showAll(req, res, next) {
//   try {
//     let roleId = 2;
//     switch (req.query.role) {
//       case 'teacher': roleId = 2; break; // TO_DO
//       case 'admin': roleId = 1; break; // TO_DO // TO_DO
//       case 'all': roleId = 0; break; // TO_DO
//       default: break;
//     }
//     const where = roleId ? { roleId } : null; 
//     const users = await models.user.findAll({
//       where,
//       include: [
//         { model: models.role },
//         { model: models.shedule },
//       ],
//     });
//     return res.responseHandler(users);
//   } catch (error) {
//     return next(error);
//   }
// }

// export async function showCurrentUser(req, res, next) {
//   try {
//     return res.responseHandler(req.user);
//   } catch (error) {
//     return next(error);
//   }
// }

// export async function update(req, res, next) {
//   try {
//     const password = crypto.createHash('sha1').update(req.body.password).digest('hex');
//     const user = await models.user.update(
//     {
//       ...req.body,
//       password,
//     },
//     {
//       where: { id: req.params.id },
//       returning: true,
//     });
//     return res.responseHandler(user);
//   } catch (error) {
//     return next(error);
//   }
// }

// export function getPermissions(req, res, next) {
//   try {
//     const arrayOfPermissions = Object.keys(permissions)
//       .filter(item => Object.prototype.hasOwnProperty.call(permissions, item))
//       .map(item => ({ name: item, value: permissions[item] }));
//     return res.responseHandler(arrayOfPermissions);
//   } catch (error) {
//     return next(error);
//   }
// }

// export async function remove(req, res, next) {
//   try {
//     const user = await models.user.destroy({
//       where: {
//         id: req.params.id,
//       },
//     });
//     return res.responseHandler(user);
//   } catch (error) {
//     return next(error);
//   }
// }

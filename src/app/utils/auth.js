import uuid from 'uuid/v4';
import jwt from 'jsonwebtoken';
import * as redis from './redis'; 
import { AuthError, AccessDenied } from './errorHandler';
import AuthRepository from '../api/auth/repository';

export async function checkAuthentication(req, res, next) {
  try {
    try {
      const { sessionId } = jwt.verify(req.headers.authorization, process.env.JWT_SECRET);
      req.sessionId = sessionId;
    } catch (error) {
      req.sessionId = uuid();
      req.permission = null;
      req.user = null;
      req.isSignIn = false;
      console.log('Authentication Failed!');
      next();
    }
    const user = new AuthRepository('user'); 
    // const user = JSON.parse(await redis.get(currentSessionId));
    if (!user) {
      throw new AuthError(); // TODO: ERROR_HANDLER
    }
    req.isSignIn = true;
    req.user = user;
    req.sessionId = currentSessionId;
    req.permission = user.role.value;
    return next();
  } catch (error) {
    
    req.sessionId = null;
    req.user = null;
    req.isSignIn = false;
    return next();
  }
}


export function checkPermissions(permission) {
  return (req, res, next) => {
    try {
      if ((permission & req.permission) === 0) {
        throw new AccessDenied();
      }
      return next();
    } catch (error) {
      return next(error);
    }
  };
}

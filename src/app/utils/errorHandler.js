export class ErrorHandler extends Error {
  constructor() {
    super();
  }
}
export class BadRequest extends ErrorHandler {
  constructor(
    message = 'The request could not be understood or was missing any required parameters.',
    errors = null,
    name = 'BAD_REQUEST',
    status = 400,
  ) {
    super();
    this.data = null;
    this.message = message;
    this.errors = errors;
    this.status = status;
    this.name = name;
  }
}

export class NotFound extends ErrorHandler {
  constructor(
    message = 'File dose not exist',
    errors = null,
    name = 'NOT_FOUND',
    status = 404,
  ) {
    super();
    this.data = null;
    this.message = message;
    this.errors = errors;
    this.status = status;
    this.name = name;
  }
}

export class AuthError extends ErrorHandler {
  constructor(
    message = 'Authentication failed or user does not have permissions for the requested operation.',
    errors = null,
    name = 'UNAUTHORIZED_ACCESS',
    status = 401,
  ) {
    super();
    this.data = null;
    this.message = message;
    this.errors = errors;
    this.status = status;
    this.name = name;
  }
}


export class AccessDenied extends ErrorHandler {
  constructor(
    message = 'Access denied',
    errors = null,
    name = 'FORBIDDEN',
    status = 403,
  ) {
    super();
    this.data = null;
    this.message = message;
    this.errors = errors;
    this.status = status;
    this.name = name;
  }
}

export class ValidationError extends ErrorHandler {
  constructor(
    message = 'The request was well-formed but was unable to be followed due to semantic errors.',
    errors = null,
    name = 'UNPROCESSABLE_ENTITY',
    status = 422,
  ) {
    super();
    this.data = null;
    this.message = message;
    this.errors = errors;
    this.status = status;
    this.name = name;
  }
}

export const PERMISSION = 1;
export const USER_VIEW = 2;
export const USER_EDIT = 4;
export const ROLE_VIEW = 8;
export const ROLE_EDIT = 16;

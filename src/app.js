import path from 'path';
import express from 'express';
import bodyParser from 'body-parser';
import expressValidator from 'express-validator';
import logger from 'morgan';
import Sequelize from 'sequelize';
import redis from 'redis';
import { setUpDb } from './app/models';
import enableRoutes from './app/index.js';
import sequelizeConfig from '../sequelizeConfig';
import { ErrorHandler } from './app/utils/errorHandler';
import { checkAuthentication } from './app/utils/auth';

class Application {
  constructor() {
    this.app = express();
    this.initApp();
  }

  initApp() {
    this.configApp();
    this.dbConfig();
    this.dbRedisConfig();
    this.setParams();
    // this.setSession();
    this.setRouter();
    this.setErrorHandler();
    this.set404Handler();
  }

  configApp() {
    this.app.use(bodyParser.json({
      limit: '5mb',
    }));
    this.app.use(bodyParser.urlencoded({
      limit: '5mb',
      extended: true,
    }));
    this.app.use(expressValidator());
    this.app.use((req, res, next) => {
      res.setHeader('Access-Control-Allow-Origin', '*');
      res.setHeader('Access-Control-Allow-Credentials', true);
      res.setHeader('Access-Control-Allow-Methods', 'GET,POST,PUT,DELETE');
      res.setHeader('Access-Control-Allow-Headers', 'Content-Type, Authorization');
      if (req.method === 'OPTIONS') {
        return res.json();
      }
      return next();
    });
    this.app.use((req, res, next) => {
      res.responseHandler = function responseHandler(data = null, message = '', status = 200, name = 'SUCCESS') {
        this.status(status).json({
          status: name,
          message,
          data,
          errors: null,
        });
      };
      return next();
    });
    this.app.use(logger('combined'));
    if (process.NODE_ENV !== 'production') {
      this.app.use('/api/assets', express.static(path.join(__dirname, '../', 'public')));
    }
  }

  dbConfig() {
    const sequelize = new Sequelize(sequelizeConfig);
    setUpDb(sequelize);
  }

  dbRedisConfig() {
    const client = redis.createClient({
      port: process.env.REDIS_PORT,
      host: process.env.REDIS_HOST,
    });
    client.on('error', (error) => {
      console.log(`Error REDIS ${error}`);
    });
    client.on('connect', () => {
      console.log('REDIS connect ready');
    });
    global.redisClient = client;
  }

  setParams() {
    this.app.set('env', process.env.NODE_ENV);
  }

  setSession() {
    this.app.use('/api', checkAuthentication);
  }

  setRouter() {
    this.router = express.Router();
    this.app.use('/api', enableRoutes(this.router));
  }

  set404Handler() {
    this.app.use((req, res) => {
      res.status(404).json({
        status: 'Error',
        message: '',
        data: null,
        errors: '',
      });
    });
  }

  setErrorHandler() {
    this.app.use((error, req, res, next) => {
      // console.error(error);
      if (error.formatWith && typeof error.formatWith === 'function') {
        return res.status(403).json({
          status: 'Error',
          message: error.message,
          data: null,
          errors: error.mapped(),
        });
      }
      console.log(error, error instanceof ErrorHandler);
      if (error instanceof ErrorHandler) {
        return res.status(error.status || 400).json({
          status: error.name,
          message: error.message,
          data: null,
          errors: error.errors,
        });
      }
      // send log errors to server TO_DO
      return res.status(500).json({
        status: 'Error',
        message: error.message,
        data: null,
        errors: null,
      });
    });
  }
}

const instanceOfApplication = new Application();
export default () => instanceOfApplication.app;
